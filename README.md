# Transport
This website is the presentation of my father's transport firm, VITAL COMIMPEX SRL. <br>
With more than 25 years of experience in the field, we offer tailor-made solutions to our customers and partners for domestic and international road transport with complete or partial vehicles, in strict compliance with the CMR Convention.